import numpy as np
from scipy.signal import resample, decimate


def _get_best_fft_lengths(old_length, new_lengths_ranges):
    """
    Tries to find optimal new lengths to resample faster (computed with fft)

    Args:
        old_length: int
        new_lengths_ranges: array, shape (ndil,2,)
            ranges of lengths among which the new lengths must be seearched

    Returns:
        new_lenths: array, shape (ndil,)

    """
    ## TODO : better !!!!
    return np.rint(np.mean(new_lengths_ranges, axis=1)).astype(int)

def _dilation_factors(Q, beta):
    """
    Computes the logarithmically spaced dilation factors
        [beta**q for q in range(-Q, Q+1)]

    Args:
        Q: int
            ~ number of coefficients
        beta: float
            resolution between dilations

    Returns:
        coefs: array, shape (2*Q+1,)
            dilation coeffitients
    """
    assert isinstance(beta, (float,int)), 'parameter "beta" must be a float'
    assert beta<=1, "can't have parameter beta>1"
    print('beta : ',beta)
    return beta**np.arange(-Q, Q+1, 1)

def _dilation_factors_max(Q, factor_max):
    """
    Computes the logarithmically spaced dilation factors
    between 1/factor_max and factor_max
        [beta**q for q in range(-Q, Q+1)]

    Args:
        Q: int
            ~ number of coefficients
        factor_max: float
            maximum dilation factor

    Returns:
        coefs: array, shape (2*Q+1,)
            dilation coeffitients
    """
    beta = factor_max**(-1/Q) if Q!=0 else 1
    return _dilation_factors(Q, beta), beta

def dilated_index(n_times, dilation_factors=None, Q=None, beta=None, factor_max=None, variability=None, verbose=False):
    """
    Signal truncated if n_times/gamma_i is not integer

    Args:
        n_times: int
            number of time indices
        dilation_factors: array, shape (n_dil,)
            see _dilation_factors()
        Q: int
            ~ number of coefficients
        beta: float
            resolution between dilations
        factor_max: float
            maximum dilation factor
        variability: None or float
            variability factor, see _get_best_fft_lengths()

    Returns:
        oldToNew_idx: array, shape (n_dil,n_times,)
            description
        newToOld_idx: array, shape (n_dil,max(lengths),)
            description
        lengths: array, shape (n_dil,)
            description
        mask: array, shape (n_dil,max(lengths),)
            description
        dilation_factors: array, shape (n_dil,)
            description
    """
    eps = 1e-10
    if dilation_factors is None:
        assert not (beta is None and factor_max is None), 'beta and factor_max can not be both None'
        if beta is None:
            exact_dilation_factors, beta = _dilation_factors_max(Q, factor_max)
        else:
            exact_dilation_factors = _dilation_factors(Q, beta)
        exact_dilation_factors = exact_dilation_factors.reshape(-1,1)

        variability = 0. if variability is None else variability
        lengths_max = np.rint(n_times * exact_dilation_factors * beta**(-variability) + eps).astype(int)
        lengths_min = np.rint(n_times * exact_dilation_factors * beta**(+variability) + eps).astype(int)
        lengths_ranges  = np.concatenate([lengths_min, lengths_max], axis=1)
        lengths_temp = _get_best_fft_lengths(n_times, lengths_ranges).ravel()
        # remove duplicates:
        lengths,last = [], None
        for l in lengths_temp:
            if l!=last:
                last = l
                lengths.append(l)
        lengths = np.array(lengths)

        dilation_factors = (lengths/float(n_times)).reshape(-1,1)
        if verbose:
            print('beta', beta**variability, beta, 1/beta**variability)
            print('lengths_range\n', lengths_ranges)
            print('dilation_factors (old,new)\n', np.concatenate([exact_dilation_factors, dilation_factors], axis=1))
            import matplotlib.pyplot as plt
            for l in lengths_ranges.T:
                plt.plot(l)
            plt.show(block=True)
    else:
        lengths = np.rint(n_times * dilation_factors + eps).astype(int).ravel()

    n_dil = len(dilation_factors)
    mask = np.zeros((n_dil, max(lengths)))
    for i,l in enumerate(lengths):
        mask[i,:l] += 1

    oldToNew_idx = np.zeros((n_dil, n_times))
    for i,l in enumerate(lengths):
        _,oldToNew_idx[i,:] = resample(np.zeros(l), n_times, np.arange(l))

    newToOld_idx = np.zeros(mask.shape) - 1
    for i,l in enumerate(lengths):
        _,newToOld_idx[i,:l] = resample(np.zeros(n_times), l, np.arange(n_times))

    if verbose:
        print('lengths\n', lengths)
        print('max lengths', max(lengths))
        print("oldToNew_idx\n", oldToNew_idx)
        print("newToOld_idx\n", newToOld_idx)
        print("mask\n", mask)
    return oldToNew_idx, newToOld_idx, lengths, mask, dilation_factors



###############################################################################

def extend_uv(uv, lengths, n_channels):
    """
    A short description.

    Args:
        uv : array, shape (n_atoms, n_channels + n_times_atom)
            description
        lengths: array, shape (n_dil,)
            description
        n_channels: int
            description

    Returns:
        uv_ext : array, shape (n_dil * n_atoms , n_channels + n_times_atom * max_dil)
            description

    """
    n_atoms, n_times_atom = uv.shape
    n_times_atom -= n_channels
    n_dil, = lengths.shape
    uv_ext = np.zeros((n_dil,n_atoms,n_channels+max(lengths)), dtype=uv.dtype,)
    uv_ext[:,:,:n_channels] = uv[:,:n_channels]
    for i,l in enumerate(lengths):
        uv_ext[i,:,n_channels:n_channels+l] = resample(uv[:,n_channels:], l, axis=1)
    return uv_ext.reshape(n_dil*n_atoms, n_channels+max(lengths))

def unextend_grad(grad, lengths, n_channels, n_times_atom):
    """
    Experimental

    Args:
        grad : array, shape (n_dil * n_atoms, n_channels, n_times_atom * max_dil)
            gradient relative to the extended dictionary
        lengths: array, shape (n_dil,)
            lengths of the dilated atoms
        n_times_atom: int
            length of the unextended atoms
        n_channels: int
            number of channels

    Returns:
        grad_unext : array, shape (n_atoms , n_channels, n_times_atom)
            gradient relative to the un-extended dictionary

    """
    n_dil_n_atoms, n_channels, n_times_atom_ext = grad.shape
    n_dil, = lengths.shape
    assert n_dil_n_atoms%n_dil == 0
    n_atoms = n_dil_n_atoms//n_dil
    grad = grad.reshape(n_dil, n_atoms, n_channels, n_times_atom_ext)
    grad_unext = np.zeros((n_dil,n_atoms,n_channels,n_times_atom), dtype=grad.dtype,)
    for grad_i, grad_unext_i, li in zip(grad, grad_unext, lengths):
        grad_unext_i[:,:,:] = resample(grad_i[:,:,:li], n_times_atom, axis=2)
    return grad_unext.sum(axis=0)



###############################################################################
## main ##
###############################################################################
if __name__=='__main__':
    from time import time
    import matplotlib.pyplot as plt
    from attr_dict import AttrDict
    from scipy import signal, stats
    from alphacsc.utils import construct_X_multi

    p = AttrDict({
        'Q' : 50,
        'factor_max' : 1.5,
        'variability' : .4,
        'n_times' : 200,
        'n_times_atom' : 20,
        'n_trials' : 1,
        'n_channels' : 1,
        'n_atoms' : 1,

        'sparsity': 1e-2,
    })
    for k,v in p.items():
        print(k,v)

    _, _, lengths_uv, mask_uv, df = dilated_index(
        p.n_times_atom, Q=p.Q, factor_max=p.factor_max, variability=p.variability)
    oldToNew_X, _, lengths_X, _, _ = dilated_index(p.n_times, dilation_factors=1/df)



    #########################
    ## create uv, z_ext1
    #########################
    ## uv ##
    gaussian = stats.norm(loc=0, scale=.3).pdf
    t = np.linspace(-1, 1, p.n_times_atom)
    v = np.stack([gaussian(t) for _ in range(p.n_atoms)], axis=0)
    u = np.random.rand(p.n_atoms, p.n_channels) + .2
    u /= np.sum(u**2, axis=1 ,keepdims=True)**.5
    uv = np.concatenate([u,v], axis=1)
    ## z_ext1 ##
    z_ext1 = np.random.rand(p.n_trials, len(lengths_uv)*p.n_atoms, p.n_times-max(lengths_uv)+1)
    z_ext1 = z_ext1*10+10
    # z_ext1[:,:,:] = np.arange(z_ext1.shape[-1]).reshape(1,1,-1)
    # sp_mask = np.ones(z.shape)
    sp_mask = np.random.rand(*z_ext1.shape) < p.sparsity
    z_ext1[~sp_mask] = 0


    #########################
    ## compute X_hat, uv_ext
    #########################
    print("extend_uv()...")
    t0 = time()
    uv_ext = extend_uv(uv, lengths_uv, n_channels=p.n_channels)
    print(f"...done in {time()-t0}s")
    print(uv.shape, uv_ext.shape)

    X_hat = construct_X_multi(z_ext1, D=uv_ext, n_channels=p.n_channels)


    fig = plt.figure()
    for i,l in enumerate(lengths_uv):
        plt.plot(
            uv_ext.reshape(len(lengths_uv),p.n_atoms , p.n_channels+max(lengths_uv))[i,0,p.n_channels:],
            label=f'uv{i} (length={l})',
            marker="" if l!=p.n_times_atom else "o")
    plt.legend()


    plt.show()
