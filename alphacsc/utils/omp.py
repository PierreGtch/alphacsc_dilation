
import warnings
from math import sqrt
import numpy as np
from scipy.signal import correlate
from scipy import sparse
from scipy import linalg
from scipy.linalg.lapack import get_lapack_funcs

from .lil import init_zeros, is_lil

from sklearn.utils import as_float_array, check_array, check_X_y

def correlate_uv_X(uv, X_i, mode='valid'):
    """Compute DtX_i, vith D = outer(u,v)

    Parameters
    ----------
    uv: array, shape = (n_atoms, n_channels + n_times_atom)
        Dictionnary
    X_i: array, shape = (n_channels, n_times)
        signal, trial i
    mode: {'valid', 'full', 'same'}
        mode parameter of scipy.signal.correlate
    Returns
    -------
    y : array, shape = (n_atoms, n_times_valid)
    """
    assert uv.ndim == 2
    n_channels, n_times = X_i.shape
    n_atoms = uv.shape[0]
    n_times_atom = uv.shape[1] - n_channels
    n_times_valid = n_times - n_times_atom + 1

    u = uv[:, :n_channels]
    v = uv[:, n_channels:]
    # X_i = X_i.expand_dims(0)
    return np.stack([correlate(np.inner(X_i.T, uk), vk, mode=mode) for uk,vk in zip(u,v)], axis=0)


def correlate_uv_uvk(uv, uv_k, n_channels, mode='full'):
    """correlate uv and uv_k

    Parameters
    ----------
    uv: array, shape = (n_atoms, n_channels + n_times_atom)
        Dictionnary
    uv_k: array, shape = (n_channels + n_times_atom)
        Dictionnary
    mode: {'full', 'valid', 'same'}
        parameter for scipy.signal.correlate
    Returns
    -------
    y : array, shape = (n_atoms, 2*n_times_atom - 1)
    """
    u, v = uv[:,:n_channels], uv[:,n_channels:]
    uk, vk = uv_k[:n_channels], uv_k[None, n_channels:]
    return correlate(v, vk, mode=mode)*np.inner(u, uk)[:,None]

def correlate_uv_uv(uv, n_channels, order='F'):
    n_atoms, n_channels_times_atom = uv.shape
    n_times_atom = n_channels_times_atom - n_channels
    # column = np.empty((n_atoms * n_times_atom, n_atoms))
    # row = np.empty((n_atoms, n_atoms * (n_times_atom-1)))
    correlations = np.empty((n_atoms*(2*n_times_atom-1), n_atoms), order=order)
    for k, uv_k in enumerate(uv):
        c = correlate_uv_uvk(uv, uv_k, n_channels, mode='full').T
        # column[:,k] = c[n_times_atom-1::-1, :].ravel()
        # row[k,:] = c[n_times_atom:, :].ravel()
        correlations[:, k] = c[::-1].ravel()
    return correlations



###############################################################################""
from sklearn.linear_model import lars_path_gram
from alphacsc.utils.dictionary import get_D

## not working
def compute_gram_from_uv(uv, n_channels, n_times_valid):
    n_atoms, n_channels_times_atom = uv.shape
    n_times_atom = n_channels_times_atom - n_channels

    DtD = np.zeros((n_atoms*n_times_valid, n_atoms*n_times_valid), order='F')
    for k, uv_k in enumerate(uv):
        c = correlate_uv_uvk(uv, uv_k, n_channels, mode='full').T
        DtD[:n_times_atom*n_atoms, k] = c[(c.shape[0]+1)//2-1::-1, :].ravel()
        DtD[k,:n_times_atom*n_atoms] = c[(c.shape[0])//2:, :].ravel()
    # copy :
    for k in range(1, n_times_valid):
        b = min(n_times_atom*n_atoms, (n_times_valid-k)*n_atoms)
        DtD[k*n_atoms:(k+1)*n_atoms, k*n_atoms:(k+n_times_atom)*n_atoms] = DtD[:n_atoms, :b]
        DtD[(k+1)*n_atoms:(k+n_times_atom)*n_atoms, k*n_atoms:(k+1)*n_atoms] = DtD[n_atoms:b, :n_atoms]
    return DtD

def mask_delta(delta, n_atoms):
    """ masks delta shifts on both side around the selected atom """
    def f(idx):
        # idx_atom = idx%n_atoms
        shift = idx//n_atoms
        s0 = shift - delta
        s1 = shift + delta + 1
        indices0 = np.arange(s0*n_atoms, idx)
        indices1 = np.arange(idx+1, s1*n_atoms)
        return np.concatenate([indices0, indices1])
    return f


def get_code_i(X_i, uv, delta, z0=None, reg=10, algo='omp'):
    """ use omp to get code z_i

    Parameters
    ----------
    X_i: array, shape = (n_channels, n_times)
        signal
    uv: array, shape = (n_atoms, n_channels + n_times_atom)
        Dictionnary
    delta: int
        number of shifts required between two detections
    reg: int
        number of atoms to detect
    algo: {'omp'|'lars'}
        algorithm to use
    Returns
    -------
    z_i: array, shape = (n_atoms, n_times_valid) | sparse lil_matrix, shape (n_atoms, n_times_valid)
        code
    """
    n_channels, n_times = X_i.shape
    n_atoms, n_channels_times_atom = uv.shape
    n_times_atom = n_channels_times_atom - n_channels
    n_times_valid = n_times - n_times_atom + 1
    assert n_times_valid>0

        # uvtX_i : shape (n_times_valid, n_atoms).T
    uvtX_i =  correlate_uv_X(uv, X_i).T.ravel()

    Gram_holder = GramShiftedUV(uv=uv, n_channels=n_channels, n_times_valid=n_times_valid)
    # Gram_holder = GramShiftedUVPrecomputed(uv, n_channels, n_times_valid=n_times_valid)
    mask_fun = mask_delta(delta, n_atoms=n_atoms)

    if algo=='omp':
        z_i = orthogonal_mp_gram(
            Gram_holder=Gram_holder, Xy=uvtX_i,
            n_nonzero_coefs=reg,
            # tol=None,
            # norms_squared=,
            copy_Gram=False,
            copy_Xy=False,
            positive=True,
            mask_fun=mask_fun,
            )

    # elif algo=='lars':
    #     z_i = lars_path_gram(
            # Xy=uvtX_i, Gram=DtD, n_samples=n_times, max_iter=500, alpha_min=0,
            # method='lasso', copy_X=False, eps=np.finfo(np.float).eps,
            # copy_Gram=False, verbose=0, return_path=False,
            # return_n_iter=False, positive=True)
    else:
        raise ValueError(f'unknown algo "{algo}"')

    z_i = z_i.reshape(n_times_valid, n_atoms).T
    if not is_lil(z0):
        return z_i
    mask = (z_i!=0)
    lil_z_i = z0.copy()
    lil_z_i[:] = 0
    lil_z_i[mask] = z_i[mask]
    return lil_z_i

##############################################################################

class GramHolderBase:
    dtype = None
    def __len__(self):
        raise NotImplementedError
    def swap(self, i, j):
        raise NotImplementedError
    def get_column(self, i, j=None):
        raise NotImplementedError

# class DummyGramHolder(GramHolderBase):
#     def __init__(self, X):
#         """
#         Parameters
#         ----------
#         X : array, shape (n_samples, n_features)
#             Input dictionary. Columns are assumed to have unit norm.
#         """
#         self.Gram = np.dot(X.T, X)
#         self.Gram = np.asfortranarray(self.Gram)
#         self.dtype = self.Gram.dtype
#         self.swap_fun, = linalg.get_blas_funcs(('swap',), (self.Gram,))
#         self.indices = np.arange(len(self.Gram))
#     def __len__(self):
#         return len(self.Gram)
#     def swap(self, i, j):
#         self.Gram[i], self.Gram[j] = self.swap_fun(self.Gram[i], self.Gram[j])
#         self.Gram.T[i], self.Gram.T[j] = self.swap_fun(self.Gram.T[i], self.Gram.T[j])
#         self.indices[i], self.indices[j] = self.indices[j], self.indices[i]
#     def get_column(self, i, j=None):
#         if j is None:
#             return self.Gram[:,i]
#         return self.Gram[:, i:j]

class GramShiftedUV(GramHolderBase):
    def __init__(self, n_channels, n_times_valid, uv=None, correlations=None, order='F'):
        assert not(uv is None and correlations is None), 'uv and correlations can not be both None'
        if correlations is not None:
            self.correlations = correlations.copy()
        else:
            self.correlations = correlate_uv_uv(uv, n_channels, order=order)
        n_atoms_2n_times_atom_1, n_atoms = self.correlations.shape
        assert n_atoms_2n_times_atom_1%n_atoms==0 and (n_atoms_2n_times_atom_1//n_atoms)%2==1
        n_times_atom = (n_atoms_2n_times_atom_1//n_atoms + 1)//2
        self.length = n_atoms*n_times_valid
        self.n_atoms = n_atoms
        self.n_times_atom = n_times_atom
        self.indices = np.arange(self.length, dtype='int64')
        self.dtype = uv.dtype
    def __len__(self):
        return self.length
    def swap(self, i, j):
        # self.Gram[i], self.Gram[j] = self.swap_fun(self.Gram[i], self.Gram[j])
        # self.Gram.T[i], self.Gram.T[j] = self.swap_fun(self.Gram.T[i], self.Gram.T[j])
        self.indices[i], self.indices[j] = self.indices[j], self.indices[i]

    def _get_unswapped_column(self, i):
        n = self.__len__()
        shift = i//self.n_atoms
        idx_atom = i%self.n_atoms
        k0 = (shift-self.n_times_atom+1)*self.n_atoms
        c0 = 0
        if k0 < 0:
            c0 = -k0
            k0 = 0
        k1 = (shift+self.n_times_atom)*self.n_atoms
        c1 = self.n_atoms*(2*self.n_times_atom-1)
        if k1 > n:
            c1 -= k1-n
            k1 = n
        out = np.zeros(n)
        out[k0:k1] = self.correlations[c0:c1, idx_atom]
        return out
    def get_column(self, i, j=None):
        if j is None:
            return self._get_unswapped_column(self.indices[i])[self.indices]
        return np.stack([self._get_unswapped_column(idx) for idx in self.indices[i:j]], axis=1)[self.indices]

class GramShiftedUVPrecomputed(GramHolderBase):
    def __init__(self, uv, n_channels, n_times_valid, order='F'):
        n_atoms, n_channels_times_atom = uv.shape
        n_times_atom = n_channels_times_atom - n_channels
        self.length = n_atoms*n_times_valid
        self.n_atoms = n_atoms
        self.n_times_atom = n_times_atom
        self.indices = np.arange(self.length)
        self.dtype = uv.dtype
        self.Gram = compute_gram_from_uv(uv, n_channels, n_times_valid=n_times_valid)
        self.swap_fun, = linalg.get_blas_funcs(('swap',), (self.Gram,))
    def __len__(self):
        return self.length
    def swap(self, i, j):
        self.Gram[i], self.Gram[j] = self.swap_fun(self.Gram[i], self.Gram[j])
        self.Gram.T[i], self.Gram.T[j] = self.swap_fun(self.Gram.T[i], self.Gram.T[j])
        self.indices[i], self.indices[j] = self.indices[j], self.indices[i]
    def get_column(self, i, j=None):
        n = self.__len__()
        if i > n:
            raise IndexError(f'index {i} out of range for array ({n,n})')
        if j is None:
            return self.Gram[:,i]
        if j > n:
            raise IndexError(f'index {j} out of range for array ({n,n})')
        return self.Gram[:,i:j]


##############################################################################
## _gram_omp and orthogonal_mp_gram forked from scikit learn
##############################################################################

premature = """ Orthogonal matching pursuit ended prematurely due to linear
dependence in the dictionary. The requested precision might not have been met.
"""
premature_lack = """ Orthogonal matching pursuit ended prematurely
due to lack of potential candidates
"""

def _gram_omp(Gram_holder:GramHolderBase, Xy, n_nonzero_coefs,
              copy_Gram=True, copy_Xy=True, return_path=False, positive=False, mask_fun=None):
    """Orthogonal Matching Pursuit step on a precomputed Gram matrix.

    This function uses the Cholesky decomposition method.

    Parameters
    ----------
    Gram_holder : object
        deriving from GramHolderBase,
        holding the Gram matrix of the input data matrix

    Xy : array, shape (n_features,)
        Input targets

    n_nonzero_coefs : int
        Targeted number of non-zero elements

    copy_Gram : bool, optional
        Whether the gram matrix must be copied by the algorithm. A false
        value is only helpful if it is already Fortran-ordered, otherwise a
        copy is made anyway.

    copy_Xy : bool, optional
        Whether the covariance vector Xy must be copied by the algorithm.
        If False, it may be overwritten.

    return_path : bool, optional. Default: False
        Whether to return every value of the nonzero coefficients along the
        forward path. Useful for cross-validation.

    positive : bool, default=False
        Restrict coefficients to be >= 0.

    mask_fun : function. Default: None
        optional function that returns which features can not anymore
        be selected after each iteration

    Returns
    -------
    gamma : array, shape (n_nonzero_coefs,)
        Non-zero elements of the solution

    idx : array, shape (n_nonzero_coefs,)
        Indices of the positions of the elements in gamma within the solution
        vector

    coefs : array, shape (n_features, n_nonzero_coefs)
        The first k values of column k correspond to the coefficient value
        for the active features at that step. The lower left triangle contains
        garbage. Only returned if ``return_path=True``.

    n_active : int
        Number of active features at convergence.
    """
    # Gram = Gram.copy('F') if copy_Gram else np.asfortranarray(Gram)
    # max_features = len(Gram_holder) if tol is not None else n_nonzero_coefs
    max_features = n_nonzero_coefs
    Gram_partial = np.empty((len(Gram_holder), max_features), dtype=Gram_holder.dtype)

    if copy_Xy or not Xy.flags.writeable:
        Xy = Xy.copy()

    min_float = np.finfo(Gram_holder.dtype).eps
    nrm2, swap = linalg.get_blas_funcs(('nrm2', 'swap'), (Gram_partial,))
    potrs, = get_lapack_funcs(('potrs',), (Gram_partial,))

    indices = np.arange(len(Gram_holder))  # keeping track of swapping
    alpha = Xy
    # tol_curr = tol_0
    # delta = 0
    gamma = np.empty(0)
    n_active = 0


    L = np.empty((max_features, max_features), dtype=Gram_holder.dtype)

    L[0, 0] = 1.
    if return_path:
        coefs = np.empty_like(L)

    while True:
        if positive:
            lam = np.argmax(alpha)
            # if alpha[lam] <= 0:
            #     warnings.warn(premature_lack, RuntimeWarning, stacklevel=3)
            #     break
        else:
            lam = np.argmax(np.abs(alpha))
        new_column = Gram_holder.get_column(lam)
        if lam < n_active or alpha[lam] ** 2 < min_float:
            # selected same atom twice, or inner product too small
            warnings.warn(premature, RuntimeWarning, stacklevel=3)
            break
        if n_active > 0:
            L[n_active, :n_active] = Gram_partial[lam, :n_active]
            linalg.solve_triangular(L[:n_active, :n_active],
                                    L[n_active, :n_active],
                                    trans=0, lower=1,
                                    overwrite_b=True,
                                    check_finite=False)
            v = nrm2(L[n_active, :n_active]) ** 2
            Lkk = new_column[lam] - v
            if Lkk <= min_float:  # selected atoms are dependent
                warnings.warn(premature, RuntimeWarning, stacklevel=3)
                break
            L[n_active, n_active] = sqrt(Lkk)
        else:
            L[0, 0] = sqrt(new_column[lam])

        Gram_partial[:,n_active] = new_column[:]
        Gram_partial[[n_active, lam]] = Gram_partial[[lam, n_active]]
        Gram_holder.swap(n_active, lam)
        indices[n_active], indices[lam] = indices[lam], indices[n_active]
        Xy[n_active], Xy[lam] = Xy[lam], Xy[n_active]
        if mask_fun is not None:
            excluded = mask_fun(lam)
            excluded = indices[excluded[(excluded<len(Gram_partial)) & (excluded>=0)]]
            Xy[excluded] = - np.inf
        n_active += 1
        old_gamma = gamma
        # solves LL'x = X'y as a composition of two triangular systems
        gamma, _ = potrs(L[:n_active, :n_active], Xy[:n_active], lower=True,
                         overwrite_b=False)
        if positive and not np.all(gamma>=0):
            n_active -= 1
            gamma = old_gamma
            warnings.warn(premature_lack, RuntimeWarning, stacklevel=3)
            break

        if return_path:
            coefs[:n_active, n_active - 1] = gamma
        beta = np.dot(Gram_partial[:, :n_active], gamma)
        alpha = Xy - beta
        # if tol is not None:
        #     tol_curr += delta
        #     delta = np.inner(gamma, beta[:n_active])
        #     tol_curr -= delta
        #     if abs(tol_curr) <= tol:
        #         break
        # elif n_active == max_features:
        if n_active == max_features:
            break
    # assert np.all(indices==Gram_holder.indices)
    # assert np.allclose(Gram_partial[:,:n_active], Gram_holder.get_column(0,n_active))
    # assert not positive or not np.sum(np.multiply(gamma<0, gamma!=0))
    assert not positive or np.all(gamma>=0)
    if return_path:
        return gamma, indices[:n_active], coefs[:, :n_active], n_active
    else:
        return gamma, indices[:n_active], n_active


def orthogonal_mp_gram(
    Gram_holder, Xy, n_nonzero_coefs=None,
    copy_Gram=True, copy_Xy=True, return_path=False,
    return_n_iter=False, positive=False, mask_fun=None):
    """Gram Orthogonal Matching Pursuit (OMP)

    Solves n_targets Orthogonal Matching Pursuit problems using only
    the Gram matrix X.T * X and the product X.T * y.

    Read more in the :ref:`User Guide <omp>`.

    Parameters
    ----------
    Gram_holder : object
        deriving from GramHolderBase,
        holding the Gram matrix of the input data matrix

    Xy : array, shape (n_features,) or (n_features, n_targets)
        Input targets multiplied by X: X.T * y

    n_nonzero_coefs : int
        Desired number of non-zero entries in the solution. If None (by
        default) this value is set to 10% of n_features.

    copy_Gram : bool, optional
        Whether the gram matrix must be copied by the algorithm. A false
        value is only helpful if it is already Fortran-ordered, otherwise a
        copy is made anyway.

    copy_Xy : bool, optional
        Whether the covariance vector Xy must be copied by the algorithm.
        If False, it may be overwritten.

    return_path : bool, optional. Default: False
        Whether to return every value of the nonzero coefficients along the
        forward path. Useful for cross-validation.

    return_n_iter : bool, optional default False
        Whether or not to return the number of iterations.

    positive : bool, default=False
        Restrict coefficients to be >= 0.

    mask_fun : function. Default: None
        optional function that returns which features can not anymore
        be selected after each iteration

    Returns
    -------
    coef : array, shape (n_features,) or (n_features, n_targets)
        Coefficients of the OMP solution. If `return_path=True`, this contains
        the whole coefficient path. In this case its shape is
        (n_features, n_features) or (n_features, n_targets, n_features) and
        iterating over the last axis yields coefficients in increasing order
        of active features.

    n_iters : array-like or int
        Number of active features across every target. Returned only if
        `return_n_iter` is set to True.

    See also
    --------
    OrthogonalMatchingPursuit
    orthogonal_mp
    lars_path
    decomposition.sparse_encode

    Notes
    -----
    Orthogonal matching pursuit was introduced in G. Mallat, Z. Zhang,
    Matching pursuits with time-frequency dictionaries, IEEE Transactions on
    Signal Processing, Vol. 41, No. 12. (December 1993), pp. 3397-3415.
    (http://blanche.polytechnique.fr/~mallat/papiers/MallatPursuit93.pdf)

    This implementation is based on Rubinstein, R., Zibulevsky, M. and Elad,
    M., Efficient Implementation of the K-SVD Algorithm using Batch Orthogonal
    Matching Pursuit Technical Report - CS Technion, April 2008.
    https://www.cs.technion.ac.il/~ronrubin/Publications/KSVD-OMP-v2.pdf

    """
    # Gram = check_array(Gram, order='F', copy=copy_Gram)
    Xy = np.asarray(Xy)
    if Xy.ndim > 1 and Xy.shape[1] > 1:
        # or subsequent target will be affected
        copy_Gram = True
    if Xy.ndim == 1:
        Xy = Xy[:, np.newaxis]
        # if tol is not None:
        #     norms_squared = [norms_squared]
    if copy_Xy or not Xy.flags.writeable:
        # Make the copy once instead of many times in _gram_omp itself.
        Xy = Xy.copy()

    if n_nonzero_coefs is None:# and tol is None:
        n_nonzero_coefs = int(0.1 * len(Gram_holder))
    # if tol is not None and norms_squared is None:
    #     raise ValueError('Gram OMP needs the precomputed norms in order '
    #                      'to evaluate the error sum of squares.')
    # if tol is not None and tol < 0:
    #     raise ValueError("Epsilon cannot be negative")
    if n_nonzero_coefs <= 0: #and tol is None
        raise ValueError("The number of atoms must be positive")
    if n_nonzero_coefs > len(Gram_holder): #and tol is None
        raise ValueError("The number of atoms cannot be more than the number "
                         "of features")
    if not positive and mask_fun is not None:
        raise ValueError("mask_fun requires positive parameter to be True")
    if return_path:
        coef = np.zeros((len(Gram_holder), Xy.shape[1], len(Gram_holder)))
    else:
        coef = np.zeros((len(Gram_holder), Xy.shape[1]))

    n_iters = []
    for k in range(Xy.shape[1]):
        out = _gram_omp(
            Gram_holder, Xy[:, k], n_nonzero_coefs,
            # norms_squared[k] if tol is not None else None, tol,
            copy_Gram=copy_Gram, copy_Xy=False,
            return_path=return_path, positive=positive, mask_fun=mask_fun)
        if return_path:
            _, idx, coefs, n_iter = out
            coef = coef[:, :, :len(idx)]
            for n_active, x in enumerate(coefs.T):
                coef[idx[:n_active + 1], k, n_active] = x[:n_active + 1]
        else:
            x, idx, n_iter = out
            coef[idx, k] = x
        n_iters.append(n_iter)

    if Xy.shape[1] == 1:
        n_iters = n_iters[0]

    if return_n_iter:
        return np.squeeze(coef), n_iters
    else:
        return np.squeeze(coef)
