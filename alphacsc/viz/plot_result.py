import matplotlib.pyplot as plt
import numpy as np
from itertools import permutations
from attr_dict import AttrDict

def plot_result(
    n_channels, n_atoms, n_trials, n_times, n_dil=1,
    max_trials=2, max_channels=3, max_atoms=5, max_times=1000,
    uv=None, uv_hat=None, X=None, X_hat=None, z=None, z_hat=None, **kwargs):
    '''
    Plots nicely the results of learn_d_z_multi or learn_d_z_multi_dilation

    Parameters
    ----------
    n_channels : int
    n_atoms :    int
    n_trials :   int
    n_times :    int
    n_dil :      int
    max_trials : int
        maximum number of trials to plot
    max_channels : int
        maximum number of channels to plot
    max_atoms : int
        maximum number of atoms to plot
    max_tomes : int
        maximum number of time samples to plot
    uv : array, shape (n_atoms, n_channels + n_times_atom)
        true dictionary (for synthetic data)
    uv_hat : array, shape (n_atoms, n_channels + n_times_atom)
        learned dictionary
    X : array, shape (n_trials, n_channels, n_times)
        true signal
    X_hat : array, shape (n_trials, n_channels, n_times)
        reconstructed signal
    z : array, shape (n_trials, n_atoms, n_times - n_times_atom + 1)
        true encoding in array format, not sparse lil (for synthetic data)
    z_hat : array, shape (n_trials, n_atoms, n_times - n_times_atom + 1)
        learned encoding in array format, not sparse lil
    '''
    data = AttrDict(X=X, X_hat=X_hat, uv=uv, uv_hat=uv_hat, z=z, z_hat=z_hat)

    n_c  = min(n_channels, max_channels)
    n_a  = min(n_atoms,    max_atoms)
    n_t  = min(n_trials,   max_trials)
    n_ti = min(n_times,    max_times)

    ## Display dict ##
    uv_col = 0
    for k in ['uv', 'uv_hat']:
        if data[k] is not None:
            uv_col += 1
            sign = np.sign(np.sum(data[k][:,:n_channels], axis=1, keepdims=True))
            data[k[1:]] = data[k][:,n_channels:] * sign
            # data[k][:,n_channels:] *= sign
    if uv_col == 2:
        ## align atoms :
        dist_mat = np.zeros((n_atoms, n_atoms))
        for i in range(n_atoms):
            for j in range(n_atoms):
                dist_mat[i,j] = np.sum((data.v[i]-data.v_hat[j])**2, axis=0)
        best_dist , best_align = np.inf, None
        for align in permutations(np.arange(n_atoms)):
            dist = np.trace(dist_mat[:,align])
            if dist<best_dist:
                best_dist = dist
                best_align = align
        data.v_hat = data.v_hat[best_align,:]
        if data.z_hat is not None:
            data.z_hat = data.z_hat[:, best_align, :]
    if uv_col != 0:
        fig = plt.figure()
        fig.suptitle("atoms (uv and uv_hat)")
        if uv is not None:
            for i in range(n_a):
                ax = plt.subplot(n_a,uv_col,i*uv_col+1)
                ax.plot(data.v[i], label=f"true atom {i}")
                plt.legend()
        if uv_hat is not None:
            for i in range(n_a):
                ax = plt.subplot(n_a,uv_col,i*uv_col+uv_col)
                ax.plot(data.v_hat[i], label=f"predicted atom {i}")
                plt.legend()

        ## plot spikes (stacked)
        fig = plt.figure()
        fig.suptitle("spikes, stacked view (uv and uv_hat)")
        if uv is not None:
            ax = plt.subplot(1,uv_col,1)
            ax.set_title("true atoms")
            for i,d in enumerate(data.v):
                amin = np.argmin(d)
                plt.plot(np.arange(d.size)-amin, d/np.abs(d[amin]), label=f'atom {i}')
            plt.legend()
        if uv_hat is not None:
            ax = plt.subplot(1,uv_col,uv_col)
            ax.set_title("predicted atoms")
            for i,d in enumerate(data.v_hat):
                amin = np.argmin(d)
                # plt.plot(np.arange(d.size)-amin, d/np.abs(d[amin]), label=f'atom {i}')
                plt.plot(np.arange(d.size)-amin, d, label=f'atom {i}')
            plt.legend()

    ## cut if signal is too long ##
    for k in ['z_hat', 'z', 'X', 'X_hat']:
        d = data[k]
        if d is not None and d.shape[2] > max_times:
            n_splits = int(np.ceil(d.shape[2]/max_times))
            new_d = np.zeros((n_trials*n_splits, d.shape[1], max_times), dtype=d.dtype)
            for i_t in range(n_trials):
                for i_s in range(n_splits):
                    slice_i = d[i_t, :, i_s*max_times:(i_s+1)*max_times]
                    new_d[i_t*n_splits + i_s, :, :slice_i.shape[-1]] = slice_i
            data[k] = new_d

    ## Display signal ##
    if not (X is None and X_hat is None):
        fig = plt.figure()
        fig.suptitle("signals (X and X_hat)")
        i_c = 0
        for i_t in range(n_t):
            ax = plt.subplot(n_t,1,i_t + 1)
            ax.set_title(f"signal{i_t}, channel{i_c}")
            if not X is None:
                ax.plot(data.X[i_t,i_c], label=f"true signal{i_t}")
            if not X_hat is None:
                ax.plot(data.X_hat[i_t,i_c], label=f"reconstructed signal{i_t}")
            plt.legend()

    ## Display code ##
    z_col = 0
    for k in ['z_hat', 'z']:
        if data[k] is not None:
            z_col += 1
            assert data[k].shape[1]%n_dil == 0
            # new_n_trials = data[k].shape[0]//n_dil
            data[k] = data[k].reshape(data[k].shape[0], n_dil, n_atoms, -1)
    if z_col != 0:
        fig = plt.figure()
        fig.suptitle("coefficients (z and z_hat)")
        # for i_t in range(n_t):
        i_t = 0
        if data.z is not None:
            # ax = plt.subplot(n_t,z_col,i_t*z_col + 1)
            ax = plt.subplot(z_col,1, 1)
            ax.set_title(f"True code | trial{i_t}")
            for i_a in range(n_atoms):
                this_z = data.z[i_t,:,i_a,:]
                mask = this_z != 0
                t = np.tile(np.arange(this_z.shape[-1]), (this_z.shape[0], 1))
                ax.plot(t[mask], this_z[mask], 'o', label=f'atom {i_a}')
            plt.legend()
        if data.z_hat is not None:
            # ax = plt.subplot(n_t,z_col,i_t*z_col + z_col)
            ax = plt.subplot(z_col,1,z_col)
            ax.set_title(f"predicted code | trial{i_t}")
            for i_a in range(n_atoms):
                this_z = data.z_hat[i_t,:,i_a,:]
                mask = this_z != 0
                t = np.tile(np.arange(this_z.shape[-1]), (this_z.shape[0], 1))
                ax.plot(t[mask], this_z[mask], 'o', label=f'atom {i_a}')
            plt.legend()

        ## another plot
        fig = plt.figure()
        fig.suptitle("coefficients full (z and z_hat)")
        # for i_t in range(n_t):
        if data.z is not None:
            # ax = plt.subplot(n_t,z_col,i_t*z_col + 1)
            ax = plt.subplot(z_col,1, 1)
            ax.set_title(f"True code | all trials")
            for i_a in range(n_atoms):
                this_z = data.z[:,:,i_a,:].transpose(1,0,2).reshape(n_dil, -1)
                mask = this_z != 0
                t = np.tile(np.arange(this_z.shape[-1]), (this_z.shape[0], 1))
                ax.plot(t[mask], this_z[mask], 'o', label=f'atom {i_a}')
            plt.legend()
        if data.z_hat is not None:
            # ax = plt.subplot(n_t,z_col,i_t*z_col + z_col)
            ax = plt.subplot(z_col,1,z_col)
            ax.set_title(f"predicted code | all trials")
            for i_a in range(n_atoms):
                this_z = data.z_hat[:,:,i_a,:].transpose(1,0,2).reshape(n_dil, -1)
                mask = this_z != 0
                t = np.tile(np.arange(this_z.shape[-1]), (this_z.shape[0], 1))
                ax.plot(t[mask], this_z[mask], 'o', label=f'atom {i_a}')
            plt.legend()

        ## an other type of plot
        fig = plt.figure()
        fig.suptitle("coefficients (z and z_hat)")
        # for i_t in range(n_t):
        i_t = 0
        for i_a in range(n_a):
            if data.z is not None:
                # ax = plt.subplot(n_t,z_col,i_t*z_col + 1)
                ax = plt.subplot(n_a,z_col,i_a*z_col + 1)
                ax.set_title(f"True code | trial{i_t} | coeffs of atom{i_a}")
                im = ax.matshow(data.z[i_t,:,i_a,:], aspect='auto')
                plt.colorbar(im)
            if data.z_hat is not None:
                # ax = plt.subplot(n_t,z_col,i_t*z_col + z_col)
                ax = plt.subplot(n_a,z_col,i_a*z_col + z_col)
                ax.set_title(f"predicted code | trial{i_t} | coeffs of atom{i_a}")
                im = ax.matshow(data.z_hat[i_t,:,i_a,:], aspect='auto')
                plt.colorbar(im)


    plt.show()
